
import time
from base64 import b64encode
import requests
import pytest
import lorem

# GLOBAL VARIABLES
editor_username = 'editor'
editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
commenter_username = 'commenter'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"
users_endpoint_url = blog_url + "/wp-json/wp/v2/users"
editor_token = b64encode(f"{editor_username}:{editor_password}".encode('utf-8')).decode("ascii")
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post Bee " + str(timestamp),
        "article_subtitle":"Article subtitle" + lorem.sentence(),
        "article_text":"Bee" + lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + editor_token
    }
    return headers


@pytest.fixture(scope='module')
def commenter_headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + commenter_token
    }
    return headers


def get_user_id(username, headers):
    response = requests.get(url=users_endpoint_url, headers=headers, params={"search": username})
    response.raise_for_status()
    users = response.json()
    for user in users:
        if user["slug"] == username:
            return user["id"]
    raise ValueError(f"User {username} not found")


@pytest.fixture(scope='module')
def editor_id(headers):
    return get_user_id(editor_username, headers)


@pytest.fixture(scope='module')
def commenter_id(commenter_headers):
    return get_user_id(commenter_username, commenter_headers)


@pytest.fixture(scope='module')
def posted_article(article, headers):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers, json=payload)
    response.raise_for_status()
    return response


# CREATE
def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"
    post_data = posted_article.json()
    assert "id" in post_data
    assert "title" in post_data
    assert post_data["title"]["rendered"].startswith("This is new post Bee ")


def test_new_post_authorship(posted_article, editor_id):
    post_data = posted_article.json()
    assert posted_article.status_code == 201
    assert post_data["author"] == editor_id
    assert post_data["status"] == "publish"


# ADD COMMENT
@pytest.fixture(scope='module')
def posted_comment(posted_article, commenter_headers):
    post_id = posted_article.json()["id"]
    payload = {
        "post": post_id,
        "content": "This is a comment Bee"
    }
    response = requests.post(url=comments_endpoint_url, headers=commenter_headers, json=payload)

    print(f"Request payload: {payload}")
    print(f"Response status code: {response.status_code}")
    print(f"Response content: {response.content.decode('utf-8')}")

    response.raise_for_status()
    return response


def test_comment_is_successfully_created(posted_comment):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"
    comment_data = posted_comment.json()
    assert "id" in comment_data
    assert "content" in comment_data
    assert comment_data["content"]["rendered"] == "<p>This is a comment Bee</p>\n"


def test_comment_relationship(posted_article, posted_comment):
    comment_data = posted_comment.json()
    assert posted_comment.status_code == 201
    assert comment_data["post"] == posted_article.json()["id"]
    assert "parent" in comment_data
    assert comment_data["parent"] == 0


def test_comment_authorship(posted_comment, commenter_id):
    comment_data = posted_comment.json()
    assert posted_comment.status_code == 201
    assert comment_data["author"] == commenter_id


# ADD REPLY TO COMMENT
@pytest.fixture(scope='module')
def posted_reply(posted_comment, headers):
    comment_id = posted_comment.json()["id"]
    post_id = posted_comment.json()["post"]
    payload = {
        "post": post_id,
        "content": "This is a reply Bee",
        "parent": comment_id
    }
    response = requests.post(url=comments_endpoint_url, headers=headers, json=payload)

    print(f"Request payload: {payload}")
    print(f"Response status code: {response.status_code}")
    print(f"Response content: {response.content.decode('utf-8')}")

    response.raise_for_status()
    return response


def test_reply_is_successfully_created(posted_reply):
    assert posted_reply.status_code == 201
    assert posted_reply.reason == "Created"
    reply_data = posted_reply.json()
    assert "id" in reply_data
    assert "content" in reply_data
    assert reply_data["content"]["rendered"] == "<p>This is a reply Bee</p>\n"


def test_reply_relationship(posted_comment, posted_reply):
    reply_data = posted_reply.json()
    assert posted_reply.status_code == 201
    assert reply_data["parent"] == posted_comment.json()["id"]


def test_reply_article_relationship(posted_article, posted_reply):
    reply_data = posted_reply.json()
    assert posted_reply.status_code == 201
    assert reply_data["post"] == posted_article.json()["id"]


def test_reply_authorship(posted_reply, editor_id):
    reply_data = posted_reply.json()
    assert posted_reply.status_code == 201
    assert reply_data["author"] == editor_id


# READ
def test_newly_created_post_can_be_read(article, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
    assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'


# UPDATE
def test_post_can_be_updated(article, posted_article, headers):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = posts_endpoint_url + "/" + str(wordpress_post_id)
    payload = {
        "excerpt": "This is new excerpt!"
    }
    updated_article = requests.patch(url=wordpress_post_url, json=payload, headers=headers)
    assert updated_article.status_code == 200
    assert updated_article.reason == "OK"
    published_article = requests.get(url=wordpress_post_url)
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{payload["excerpt"]}</p>\n'
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'


# DELETE
def test_post_can_be_deleted(posted_article, headers):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = posts_endpoint_url + "/" + str(wordpress_post_id)
    wordpress_delete_post_url = wordpress_post_url + "?force=true"
    deleted_article = requests.delete(url=wordpress_delete_post_url, headers=headers)
    assert deleted_article.status_code == 200
    assert deleted_article.reason == "OK"
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 404
    assert published_article.reason == "Not Found"
